Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libobjcryst
Source: https://github.com/diffpy/libobjcryst
Files-Excluded: src/cctbx
                src/newmat

Files: *
Copyright: 2000-2009 Vincent Favre-Nicolin vincefn@users.sourceforge.net
           2007 Julien Pommier
           2015 Brookhaven Science Associates
License: GPL-2+

Files: src/ObjCryst/Quirks/sse_mathfun.h
Copyright: 2007  Julien Pommier
License: Zlib

Files: debian/*
Copyright: 2021 Neil Williams <codehelp@debian.org>
License: GPL-2+

License: Zlib
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
 .
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
